<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{	
		$datos = [
					"titulo" => "Inicio de codeigniter 4"
				];
		return view('welcome_message', $datos);
	}

	public function contacto (){
		//return "Hola mundo";
		return view('contactoPrueba');
	}

	public function formulario(){
		return view('formulario');
	}

	public function enviarPost(){
		//print_r($_POST);
		$valor1 = $_POST['valor1'];
		$valor2 = $_POST['valor2'];

		echo $valor1 + $valor2;
	}

	public function inicio(){
		return view('inicio');
	}

	public function contactos(){
		return "Contenido de la vista";
	}
}
